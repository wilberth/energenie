# Energenie

script voor aansturing van energenie usb-relay. Alternatief: https://github.com/xypron/pysispm.

Installatie

- install python 3, 32 bit
- install libusb from libusb.info: copy MS32\dll\libusb-1.0.dll -> C:\Windows\SysWOW64
- pip install pyusb
- zet energenie.py ergens neer waar je hem kunt vinden, bijvoorbeeld C:\Users\TSG-Admin\Documents\Energenie

Instellen

- gpedit.msc -> User Configuration -> Windows Settings -> Scripts (Logon/Logoff)
  - python C:\Users\TSG-Admin\Documents\Energenie\energenie.py --delay 3 --allon  
  - python C:\Users\TSG-Admin\Documents\Energenie\energenie.py --delay 3 --alloff 
