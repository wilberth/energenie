#!/usr/bin/env python3
# gpedit.msc -> User Configuration -> Windows Settings -> Scripts (Logon/Logoff)
# python C:\Users\TSG-Admin\Documents\Energenie\energenie.py --delay 3 --allon 
# python C:\Users\TSG-Admin\Documents\Energenie\energenie.py --delay 3 --alloff 
import sys
import time
import getopt
import usb
class PM(object):
    """
    powermanager Energenie aka sispm
    windows compatible class
    """
    def __init__(self):
        self.dev = usb.core.find(idVendor=0x04b4, idProduct=0xfd15)
        
    def id(self):
        return (usb.util.get_string(self.dev, 1), usb.util.get_string(self.dev, 2))
        
    def status(self, i):
        buf = bytes([3 * i, 0x03, 0x00, 0x00, 0x00])
        buf = self.dev.ctrl_transfer(0xa1, 0x01, 0x0300 + 3 * i, 0, buf, 500)
        return buf[1]==11
        
    def off(self, i):
        buf = bytes([3 * i, 0x00, 0x00, 0x00, 0x00])
        self.dev.ctrl_transfer(0x21, 0x09, 0x0300 + 3 * i, 0, buf, 500)

    def on(self, i):
        buf = bytes([3 * i, 0x03, 0x00, 0x00, 0x00])
        self.dev.ctrl_transfer(0x21, 0x09, 0x0300 + 3 * i, 0, buf, 500)

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "n:f:NFd:", ["on=", "off=", "allon", "alloff", "delay="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    pm = PM()
    if len(opts) == 0:
        print("usage: energenie --status [i] --on [i] --off [i] --allon --alloff [--delay n]")
    delay = 1 # second
    for o, a in opts:
        if o in ("-n", "--on"):
            pm.on(int(a))
        elif o in ("-f", "--off"):
            pm.off(int(a))
        elif o in ("-N", "--allon"):
            for i in [1,2,3,4]:
                pm.on(i)
                time.sleep(delay)
        elif o in("-F", "--alloff"):
            for i in [4,3,2,1]:
                pm.off(i)
                time.sleep(delay)
        elif o in("-d", "--delay"):
            delay = float(a)
        else:
            assert False, "unhandled option"
    print(pm.id())
    for i in [1,2,3,4]:
        print("status {}: {}".format(i, pm.status(i)))
    
if __name__ == "__main__":
    main()
